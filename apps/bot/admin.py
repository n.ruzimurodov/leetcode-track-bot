from django.contrib import admin
from . import models


@admin.register(models.TelegramProfile)
class TelegramProfileAdmin(admin.ModelAdmin):
    list_display = ("id", "username", "telegram_id")
