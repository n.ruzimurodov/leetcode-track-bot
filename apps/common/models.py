from django.db import models


class TimeStampedModel(models.Model):
    """Abstract model for timestamped models."""

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        """Meta options."""

        abstract = True
