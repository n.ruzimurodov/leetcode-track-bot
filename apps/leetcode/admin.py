from django.contrib import admin
from . import models


@admin.register(models.Group)
class GroupAdmin(admin.ModelAdmin):
    list_display = ("id", "title", "creator", "username", "telegram_id", "is_active")
    list_filter = ("is_active",)


@admin.register(models.GroupMember)
class GroupMemberAdmin(admin.ModelAdmin):
    list_display = ("id", "group", "user", "username", "coins", "is_active")
    list_filter = ("is_active",)


@admin.register(models.GroupMemberCoin)
class GroupMemberCoinAdmin(admin.ModelAdmin):
    list_display = ("member", "coin", "reason", "is_active")
    list_filter = ("is_active",)


@admin.register(models.DailyCheckResult)
class DailyCheckResultAdmin(admin.ModelAdmin):
    list_display = ("member", "result")
    list_filter = ("created_at","member")
