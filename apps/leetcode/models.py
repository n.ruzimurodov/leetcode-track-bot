from django.db import models
from apps.common.models import TimeStampedModel
from apps.bot.models import TelegramProfile
from django.utils.translation import gettext_lazy as _


class Group(TimeStampedModel):
    title = models.CharField(max_length=255)
    creator = models.ForeignKey(TelegramProfile, on_delete=models.CASCADE, related_name="created_groups")
    username = models.CharField(max_length=255, null=True, blank=True)
    telegram_id = models.BigIntegerField(null=True, blank=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.title


class GroupMember(TimeStampedModel):
    group = models.ForeignKey(Group, on_delete=models.CASCADE, related_name="members")
    user = models.ForeignKey(TelegramProfile, on_delete=models.CASCADE, related_name="groups")
    username = models.CharField(max_length=255, verbose_name=_("Leetcode Username"))
    coin = models.IntegerField(default=0)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.username


class CoinReason(models.TextChoices):
    daily = "daily", _("Daily")
    second = "second", _("Second")


class GroupMemberCoin(TimeStampedModel):
    member = models.ForeignKey(GroupMember, on_delete=models.CASCADE, related_name="coins")
    coin = models.IntegerField(default=0)
    reason = models.CharField(max_length=255, choices=CoinReason.choices, default=CoinReason.daily)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return str(self.member.username) + " " + str(self.coin)


class DailyCheckResult(TimeStampedModel):
    member = models.ForeignKey(GroupMember, on_delete=models.CASCADE, related_name="daily_check_results")

    result = models.JSONField(
        default=dict,
        verbose_name=_("Daily Check Result")
    )
