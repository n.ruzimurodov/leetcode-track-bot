from core import celery_app
from apps.leetcode.models import GroupMember, DailyCheckResult
import requests


@celery_app.task
def check_user_result(groupmember_id):
    groupmember = GroupMember.objects.get(id=groupmember_id)
    url = f"https://leetcode-stats-api.herokuapp.com/{groupmember.username}"
    response = requests.get(url)
    if response.ok:
        data = response.json()
        DailyCheckResult.objects.create(
            member=groupmember,
            result=data
        )


@celery_app.task
def check_all_users_result():
    groupmembers = GroupMember.objects.all()
    for groupmember in groupmembers:
        check_user_result.delay(groupmember.id)
